<?php
	
	header("Content-type: text/css; charset: UTF-8");
	
	require_once('../../../../wp-load.php');
	
	$options = get_option('sf_pinpoint_options');
	
		
	// Font
	$standard_font = $options['web_standard_font'];
	$heading_font = $options['web_heading_font'];
	$use_custom_font_one = $options['use_custom_font_one'];
	$custom_font_one = explode(':', $options['standard_font']);
	$use_custom_font_two = $options['use_custom_font_two'];
	$custom_font_two = explode(':', $options['heading_font']);
	$use_custom_font_impact = $options['use_custom_font_impact'];
	$custom_font_impact = explode(':', $options['impact_font']);
	$google_font_one = str_replace("+", " ", $custom_font_one[0]);
	$google_font_two = str_replace("+", " ", $custom_font_two[0]);
	$google_font_impact = str_replace("+", " ", $custom_font_impact[0]);
	
	// Custom CSS
	$custom_css = $options['custom_css'];

?>

/* Standard Styles
================================================== */

/*========== Custom Font Styles ==========*/

body, h6, #sidebar .widget-heading h3, #header-search input, .header-items h3.phone-number, .related-wrap h3, #comments-list > h3, .item-heading h1, .button, button, .sf-button, input[type="submit"], input[type="email"], input[type="reset"], input[type="button"], .wpb_accordion_section h3, #header-login input {
	font-family: "Coda", Helvetica, Arial, Tahoma, sans-serif;
}
h1, h2, h3, h4, h5, .custom-caption p, span.dropcap1, span.dropcap2, span.dropcap3, span.dropcap4, .wpb_call_text, .impact-text, .testimonial-text {
	font-family:  "Coda", Helvetica, Arial, Tahoma, sans-serif;
}

